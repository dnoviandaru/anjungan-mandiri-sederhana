<?php
    require_once("./proses.php");
    setlocale(LC_ALL, 'id-ID', 'id_ID');
?>

<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol.lst-kix_vx76ajlvg2mf-4.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-4 0
        }

        ol.lst-kix_vx76ajlvg2mf-2.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-2 0
        }

        .lst-kix_vx76ajlvg2mf-3>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-3, decimal) ". "
        }

        .lst-kix_vx76ajlvg2mf-2>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-2, lower-roman) ". "
        }

        .lst-kix_vx76ajlvg2mf-4>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-4, lower-latin) ". "
        }

        ol.lst-kix_vx76ajlvg2mf-6.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-6 0
        }

        ol.lst-kix_vx76ajlvg2mf-8 {
            list-style-type: none
        }

        ol.lst-kix_vx76ajlvg2mf-7 {
            list-style-type: none
        }

        ol.lst-kix_vx76ajlvg2mf-2 {
            list-style-type: none
        }

        ol.lst-kix_vx76ajlvg2mf-1 {
            list-style-type: none
        }

        .lst-kix_vx76ajlvg2mf-8>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-8
        }

        .lst-kix_vx76ajlvg2mf-0>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-0, decimal) ". "
        }

        ol.lst-kix_vx76ajlvg2mf-0 {
            list-style-type: none
        }

        .lst-kix_vx76ajlvg2mf-1>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-1, lower-latin) ". "
        }

        ol.lst-kix_vx76ajlvg2mf-1.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-1 0
        }

        .lst-kix_vx76ajlvg2mf-2>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-2
        }

        ol.lst-kix_vx76ajlvg2mf-6 {
            list-style-type: none
        }

        ol.lst-kix_vx76ajlvg2mf-5 {
            list-style-type: none
        }

        ol.lst-kix_vx76ajlvg2mf-4 {
            list-style-type: none
        }

        .lst-kix_vx76ajlvg2mf-5>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-5
        }

        ol.lst-kix_vx76ajlvg2mf-3 {
            list-style-type: none
        }

        .lst-kix_vx76ajlvg2mf-3>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-3
        }

        .lst-kix_vx76ajlvg2mf-7>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-7, lower-latin) ". "
        }

        ol.lst-kix_vx76ajlvg2mf-8.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-8 0
        }

        .lst-kix_vx76ajlvg2mf-6>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-6, decimal) ". "
        }

        .lst-kix_vx76ajlvg2mf-5>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-5, lower-roman) ". "
        }

        .lst-kix_vx76ajlvg2mf-6>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-6
        }

        li.li-bullet-0:before {
            margin-left: -18pt;
            white-space: nowrap;
            display: inline-block;
            min-width: 18pt
        }

        .lst-kix_vx76ajlvg2mf-1>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-1
        }

        ol.lst-kix_vx76ajlvg2mf-3.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-3 0
        }

        .lst-kix_vx76ajlvg2mf-4>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-4
        }

        .lst-kix_vx76ajlvg2mf-7>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-7
        }

        ol.lst-kix_vx76ajlvg2mf-7.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-7 0
        }

        ol.lst-kix_vx76ajlvg2mf-0.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-0 0
        }

        ol.lst-kix_vx76ajlvg2mf-5.start {
            counter-reset: lst-ctn-kix_vx76ajlvg2mf-5 0
        }

        .lst-kix_vx76ajlvg2mf-8>li:before {
            content: ""counter(lst-ctn-kix_vx76ajlvg2mf-8, lower-roman) ". "
        }

        .lst-kix_vx76ajlvg2mf-0>li {
            counter-increment: lst-ctn-kix_vx76ajlvg2mf-0
        }

        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c13 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 137.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c7 {
            border-right-style: solid;
            padding: 2.2pt 2.2pt 2.2pt 2.2pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 102pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c9 {
            border-right-style: solid;
            padding: 2.2pt 2.2pt 2.2pt 2.2pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 366pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c8 {
            border-right-style: solid;
            padding: 2.2pt 2.2pt 2.2pt 2.2pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 306.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c3 {
            border-right-style: solid;
            padding: 2.2pt 2.2pt 2.2pt 2.2pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 161.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c14 {
            padding-top: 0pt;
            text-indent: 36pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c2 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c5 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c10 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c4 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c1 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }

        .c12 {
            margin-left: 330.8pt;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c16 {
            background-color: #ffffff;
            max-width: 468pt;
            padding: 72pt 72pt 72pt 72pt
        }

        .c11 {
            margin-left: 36pt;
            padding-left: 0pt
        }

        .c17 {
            padding: 0;
            margin: 0
        }

        .c6 {
            height: 11pt
        }

        .c15 {
            text-indent: 36pt
        }

        .c0 {
            height: 0pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c16">
    <p class="c5"><span class="c2">Perihal : Permohonan Kuasa Insidentil</span></p>
    <p class="c5 c6"><span class="c2"></span></p>
    <p class="c5"><span class="c2">Yogyakarta, <?php echo $tanggal; ?></span></p>
    <p class="c5"><span class="c2">Kepada Yth,</span></p>
    <p class="c5"><span class="c2">Ketua Pengadilan Negeri Hubungan Industrial Dan</span></p>
    <p class="c5"><span class="c2">Tindak Pidana Korupsi Yogyakarta Kelas IA</span></p>
    <p class="c5"><span class="c2">Di-tempat</span></p>
    <p class="c5 c6"><span class="c2"></span></p>
    <p class="c5"><span class="c2">Dengan hormat,</span></p>
    <p class="c5"><span class="c2">Yang bertanda tangan dibawah ini :</span></p>
    <p class="c5 c6"><span class="c2"></span></p><a id="t.649370fb7cdac40f52a6f459941c58de8f349e4e"></a><a id="t.0"></a>
    <table class="c10">
        <tbody>
            <tr class="c0">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Nama</span></p>
                </td>
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">: <?php echo $nama ?></span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">NIK</span></p>
                </td>
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">: <?php echo $nik ?></span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Alamat</span></p>
                </td>
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">: <?php echo $alamat ?></span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Agama</span></p>
                </td>
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">: <?php echo $agama ?></span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Kewarganegaraan</span></p>
                </td>
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">: Indonesia</span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Setatus Hubungan Keluarga</span></p>
                </td>
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">: <?php echo $statusHubungan ?></span></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="c5 c6"><span class="c2"></span></p>
    <p class="c5"><span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dengan ini saya mengajukan permohonan
            untuk dapat menjadi Kuasa Insidentil untuk beracara di Pengadilan Negeri, Hubungan Industrial dan Tindak
            Pidana Koruspi Yogyakarta Kelas IA mewakili</span></p>
    <p class="c5 c6"><span class="c2"></span></p><a id="t.d09a235d97201352d546c18c88a2bc3f86c9b71d"></a><a id="t.1"></a>
    <table class="c10">
        <tbody>
            <tr class="c0">
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Nama</span></p>
                </td>
                <td class="c9" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">:</span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">NIK</span></p>
                </td>
                <td class="c9" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">:</span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Alamat</span></p>
                </td>
                <td class="c9" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">:</span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Pekerjaan</span></p>
                </td>
                <td class="c9" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">:</span></p>
                </td>
            </tr>
            <tr class="c0">
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">Agama</span></p>
                </td>
                <td class="c9" colspan="1" rowspan="1">
                    <p class="c4"><span class="c2">:</span></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="c5 c6"><span class="c2"></span></p>
    <p class="c5"><span class="c2">Dalam sidang &hellip;</span></p>
    <p class="c5 c6"><span class="c2"></span></p>
    <p class="c5"><span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sebagai bahan pertimbangan Ketua
            Pengadilan Negeri, Hubungan Industrial dan Tindak Pidana Koruspi Yogyakarta Kelas IA, bersama ini saya
            lampirkan :</span></p>
    <ol class="c17 lst-kix_vx76ajlvg2mf-0 start" start="1">
        <li class="c5 c11 li-bullet-0"><span class="c2">Surat Keterangan Hubungan Keluarga dari RT, RW yang diketahui
                oleh Lurah/Desa, Kecamatan;</span></li>
        <li class="c5 c11 li-bullet-0"><span class="c2">Surat Kuasa dari Pemberi Kuasa dengan ditandatangani oleh
                pemberi dan penerima kuasa (bermaterai);</span></li>
        <li class="c5 c11 li-bullet-0"><span class="c2">Foto copy SKCK Penerima Kuasa yang sudah dilegalisir;</span>
        </li>
        <li class="c5 c11 li-bullet-0"><span class="c2">Foto copy KTP dan KK Pemberi Kuasa;</span></li>
        <li class="c5 c11 li-bullet-0"><span class="c2">Foto copy KTP dan KK Penerima Kuasa;</span></li>
        <li class="c5 c11 li-bullet-0"><span class="c2">Foto 4x6 sebanyak 2 lembar;</span></li>
    </ol>
    <p class="c5 c6"><span class="c2"></span></p>
    <p class="c5 c15"><span class="c2">Demikian permohonan ini dibuat, atas terkabulnya permohonan saya ini, saya
            ucapkan banyak terima kasih.</span></p>
    <p class="c5 c6 c15"><span class="c2"></span></p>
    <p class="c6 c14"><span class="c2"></span></p><a id="t.494603a5292a720d28e846106f850b0d9fe20a08"></a><a
        id="t.2"></a>
    <table class="c12" width="250px">
        <tbody>
            <tr class="c0">
                <td class="c13" colspan="1" rowspan="1">
                    <p class="c1"><span class="c2">Pemohon,</span></p>
                    <p class="c1 c6"><span class="c2"></span></p>
                    <p class="c1 c6"><span class="c2"></span></p>
                    <p class="c4 c6"><span class="c2"></span></p>
                    <p class="c4 c6"><span class="c2"></span></p>
                    <p class="c4 c6"><span class="c2"></span></p>
                    <p class="c1"><span class="c2"><?php echo $nama ?></span></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="c5 c6"><span class="c2"></span></p>
</body>
    <script>
        window.print();
        window.onafterprint = function(e){
            document.location.href = "index.php"; 
        };
    </script>
</html>